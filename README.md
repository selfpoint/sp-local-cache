# sp-local-cache

To manage local cache of data by id, get the data using the angular service or the angular filter.

#### `SpLocalCache` Service

* `getLocalData(dataId, [cacheName='sp-local-cache-default'|options], [options]) => Promise<*>|*`
This function will return a promise of the locally stored data of the given data id in the given cache name.
When the given id has no stored data, it will automatically be fetched and resolved in the promise.

    The options are:
    * `{boolean} [doNotReturnPromise]`
    When sending `doNotReturnPromise=true` the data will be returned directly without a promise (will return `undefined` when the id has no data), the data will still be automatically fetched so it will have data in future calls.
    * `{boolean} [doNotFetchData]`
    When sending `doNotFetchData=true` the data will not be fetched when it has no local value.


* `resetLocalCache([cacheName='sp-local-cache-default'])`
This function will empty the local cache of the given cache name.

##### `SpLocalCacheProvider`

In order for the service to work the `fetchData` Injection Function must be implemented on the provider.
The service expects it to be overwritten and will throw an error when it tries to fetch data and this is not implemented.

The `fetchData`|`fetchDataByName[cacheName]` is an angularjs [Injection Function](https://docs.angularjs.org/api/auto/service/$injector) and will be injected with the following values:
* `{Array<string|number>} dataIds` - the ids of the data to fetch
* `{string} cacheName` - the name of the cache

The function should return either an array of objects or a promise that will resolve it. Each object must contain `id` property for the service to recgnize it.

Example:
```
app.config(['SpLocalCacheProvider', function(SpLocalCacheProvider) {
    SpLocalCacheProvider.fetchData = ['dataIds', 'cacheName', function(dataIds, cacheName) {
        // uses the cache name
        if (cacheName === 'people') {
            return People.get(dataIds); // => Promise<Array<{id: number, name: string}>>
        }

        // returns an array of obejcts with just the id
        return dataIds.map(function(id) {
            return {id: id};
        });
    }];
}]);
```

#### `spLocalCache` Filter

This filter allowes you to get the local data using an angularjs filter.
The filter accepts to receive the data id as the main param, it can also accept the cache name as the second param.

Example:
```
<div>{{(1 | spLocalCache).title}}</div> <!--using the default cache name-->
<div>{{(2 | spLocalCache:'people').name}}</div> <!--using a cache name-->
```