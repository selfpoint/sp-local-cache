(function (angular, app) {
    'use strict';

    angular.module('spLocalCache').filter('spLocalCache', ['SpLocalCache', SpLocalCacheFilter]);

    function SpLocalCacheFilter(SpLocalCache) {
        return function(dataId, cacheName) {
            var data = SpLocalCache.getLocalData(dataId, cacheName, {
                doNotReturnPromise: true
            });

            if (!data || data.then && angular.isFunction(data.then)) {
                return;
            }

            return data;
        };
    }
})(angular);
